const util = require('util');
const fs = require('fs')
const path = require('path');
const readFileAsync = util.promisify(fs.readFile);


function replaceHttpWithHttps(data) {
    return data.replace('http://www.googletagmanager.com', 'https://www.googletagmanager.com')
        .replace('http://www.googleadservices.com', 'https://www.googleadservices.com')
        .replace('http://www.google-analytics.com', 'https://www.google-analytics.com')
        .replace('http://googleads.g.doubleclick.net', 'https://googleads.g.doubleclick.net')
        .replace('http://fonts.googleapis.com', 'https://fonts.googleapis.com')
        .replace('http://fonts.gstatic.com', 'https://fonts.gstatic.com')
}


async function httpToHttps() {

    function filewalker(dir, done, extension) {
        let results = []
        fs.readdir(dir, function (err, list) {
            if (err) {
                return done(err)
            }

            let pending = list.length
            if (!pending) {
                return done(null, results)
            }

            list.forEach(function (file) {

                file = path.resolve(dir, file)
                fs.stat(file, function (err, stat) {

                    if (stat && stat.isDirectory()) {

                        filewalker(file, function (err, res) {
                            results = results.concat(res)
                            if (!--pending) {
                                return done(null, results)
                            }
                        }, extension)
                    } else {
                        if (file.slice(-5) === extension) {
                            results.push(file)
                        }

                        if (!--pending) {
                            return done(null, results)
                        }
                    }
                })
            })
        })
    }

    filewalker(__dirname + '/build', function (err, data) {
        if (err) {
            throw err;
        }
        data.forEach(async (filePath) => {
            try {
                const file = await readFileAsync(filePath)
                const newValue = replaceHttpWithHttps(file.toString())
                fs.writeFileSync(filePath, newValue)
            } catch (err) {
                console.log(err)
            }
        })

    }, '.html');


}

httpToHttps()