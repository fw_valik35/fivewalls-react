#!/bin/bash

# Exit on error
set -e

# Create backup of existing deployment
sudo tar -czf /var/www/html-backup-$(date +%Y%m%d_%H%M%S).tar.gz -C /var/www/html .

# Clear destination directory
sudo rm -rf /var/www/html/*

# Extract new files
sudo tar -xzvf /tmp/package-${BITBUCKET_BUILD_NUMBER}.tar.gz -C /var/www/html

# Set permissions
sudo chown -R www-data:www-data /var/www/html
sudo chmod -R 755 /var/www/html

# Cleanup
rm -f /tmp/package-${BITBUCKET_BUILD_NUMBER}.tar.gz