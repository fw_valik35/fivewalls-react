import DocumentService from './DocumentService';
import {Review} from '../models/Review';

export default class ReviewService extends DocumentService<Review> {

    public async index(): Promise<Review[]> {
        return super.index({queryBy: 'document.type', value: 'review'});
    }

    protected makeModel(data: any) {
        return new Review(data);
    }
}
