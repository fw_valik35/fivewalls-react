interface IPrismicApiService<Model> {

    index(params?: object | string): Promise<Model[]>;
}

export default IPrismicApiService;
