import {usePrismicApi} from './usePrismicApi';
import ReviewService from '../../api/services/ReviewService';
import {Review} from '../../api/models/Review';

export const useReviewsApi = () => {
    const reviewService = new ReviewService();

    return usePrismicApi<Review>({
        apiService: reviewService,
        params: {  }
    });
};
