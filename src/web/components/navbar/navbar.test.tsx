import React from 'react';
import {render, screen} from '@testing-library/react';
import Navbar from './navbar';
import {Provider} from "react-redux";
import store from "../../redux";
import {BrowserRouter} from "react-router-dom";


describe('Navbar component', () => {

    it('renders without crashing', async () => {
        render(
            <Provider store={store}>
                <BrowserRouter>
                    <Navbar/>
                </BrowserRouter>
            </Provider>
        );
        const logo = screen.getByAltText(/Company logo/i);
        expect(logo).toBeInTheDocument();
    });

});