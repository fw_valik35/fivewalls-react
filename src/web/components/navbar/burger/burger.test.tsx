import React from 'react';
import {render, screen} from '@testing-library/react';
import Burger from './burger';
import userEvent from "@testing-library/user-event";
import {Provider} from "react-redux";
import store from "../../../redux";

describe('Burger component', () => {

    it('should toggle isActive class name after each click', async () => {
        render(
            <Provider store={store}>
                <Burger/>);
            </Provider>
        );
        const btn = screen.getByRole('button');
        expect(btn.classList).not.toContain('isActive');
        await userEvent.click(btn);
        expect(btn.classList).toContain('isActive');
        await userEvent.click(btn);
        expect(btn.classList).not.toContain('isActive');
    });

});