import React from 'react';
import {render, screen, cleanup} from '@testing-library/react';
import AnimatedBtn from './animatedBtn';
import userEvent from "@testing-library/user-event";

describe('AnimatedBtn component', () => {

    afterEach(cleanup);

    let btn:any;
    beforeEach(async ()=>{
        render(<AnimatedBtn btnLabel={'Get in touch'} />);
        btn = screen.getByText(/Get in touch/i);
        await userEvent.click(btn);
    });

    it('should open modal on click', () => {
        expect(screen.getByText(/Contact us/i)).toBeInTheDocument();
        // expect(screen.getByRole(/dialog/i)).toBeInTheDocument();
    });

    it('should close modal on click outside modal window',  async() => {
        await userEvent.click(document.body);
        // should not check right away, need to give React a time for re-render
        setTimeout(()=>{
            expect(screen.queryByText(/Contact us/i)).not.toBeInTheDocument();
            // expect(screen.queryByRole(/dialog/i)).not.toBeInTheDocument();
        },100);
    });

});