import React from 'react';
import {render, cleanup, fireEvent, screen, getNodeText} from '@testing-library/react';
import userEvent from "@testing-library/user-event";
import FormBody from './formBody';

describe('FormBody component', () => {

    // afterEach(cleanup);

    const {container, getByLabelText, getByText, getAllByText} = render(<FormBody onResponse={() => null} onClose={() => null}/>)
    const submitButton = getByText(/SUBMIT/i)

    it('should render without crashing', () => {
        expect(container).toBeInTheDocument()
    })

    it('should not fetch data if one of mandatory fields is empty',  async() => {
        // const personalDataCheckbox = getByLabelText(/store and process my personal data/i)
        // const submitButton = getByText(/SUBMIT/i)

        await userEvent.click(submitButton)

        const emailInput = getByLabelText(/email/i)
        // const emailInput = rendered.getByLabelText(/email/i)
const reqDiv = getAllByText(/Please complete this required field/i)

        // expect(getByText(/Please complete this required field/i)).toBeInTheDocument()
        // expect(emailInput.getElementsByTagName('div')[0]).toBeInTheDocument()
        expect(reqDiv[0].style['display']).toEqual('block')
    })

    // it('should change form state on change inputs', () => {
    //     render(<FormBody onResponse={() => null} onClose={() => null}/>)
    //     const testValue = 'test_value'
    //
    //     const emailInput = screen.getByLabelText(/email/i) as HTMLInputElement
    //     fireEvent.change(emailInput, {target: {value: testValue}})
    //
    //     // expect(container.)
    //
    //     expect(emailInput.value).toEqual(testValue)
    //
    //     const firstNameInput = screen.getByLabelText(/First name/i) as HTMLInputElement
    //     fireEvent.change(firstNameInput, {target: {value: testValue}})
    //     expect(firstNameInput.value).toEqual(testValue)

    // const emailInput = screen.getByLabelText(/email/i) as HTMLInputElement
    // fireEvent.change(emailInput, {target: {value: testValue}})
    // expect(emailInput.value).toEqual(testValue)
    //
    // const emailInput = screen.getByLabelText(/email/i) as HTMLInputElement
    // fireEvent.change(emailInput, {target: {value: testValue}})
    // expect(emailInput.value).toEqual(testValue)
    // })


    // it('should render success message when isSuccess equals true', () => {
    //     const mockSuccess = true
    //     const {getByText, container} = render(<FormMessage onBtnClick={()=>null} isSuccess={mockSuccess}/>)
    //     expect(getByText(/Got it!/i)).toBeInTheDocument()
    //     expect(container.getElementsByTagName('svg')[0].getAttribute('data-icon')).toBe('check-circle')
    // })
    //
    // it('should render failure message when isSuccess is false', () => {
    //     const mockSuccess = false
    //     const {getByText, container} = render(<FormMessage onBtnClick={()=>null} isSuccess={mockSuccess}/>)
    //     expect(getByText(/Oh, Snap!../i)).toBeInTheDocument()
    //     expect(container.getElementsByTagName('svg')[0].getAttribute('data-icon')).toBe('times-circle')
    // })

})
