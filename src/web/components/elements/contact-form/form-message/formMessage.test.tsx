import React from 'react';
import {render} from '@testing-library/react';
import FormMessage from './formMessage';

describe('FormMessage component', () => {

    it('should render success message when isSuccess equals true', () => {
        const mockSuccess = true;
        const {getByText, container} = render(<FormMessage onBtnClick={() => null} isSuccess={mockSuccess}/>);
        expect(getByText(/Got it!/i)).toBeInTheDocument();
        expect(container.getElementsByTagName('svg')[0].getAttribute('data-icon')).toBe('check-circle');
    });

    it('should render failure message when isSuccess is false', () => {
        const mockSuccess = false;
        const {getByText, container} = render(<FormMessage onBtnClick={() => null} isSuccess={mockSuccess}/>);
        expect(getByText(/Oh, Snap!../i)).toBeInTheDocument();
        expect(container.getElementsByTagName('svg')[0].getAttribute('data-icon')).toBe('times-circle');
    });

});
